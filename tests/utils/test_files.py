import datetime
from utils.files import get_info_from_filename
import pytest


@pytest.mark.parametrize(
    "filename,date,name,tags",
    [
        (
            "20000101T000000--general__exercise_health_sports.org",
            "20000101T000000",
            "general",
            ['exercise', 'health', 'sports'],
        ),
        (
            "20000101T000000--general__psychology.org",
            "20000101T000000",
            "general",
            ['psychology'],
        ),
        (
            "20000101T000000--books__hobby_personal.org",
            "20000101T000000",
            "books",
            ['hobby', 'personal'],
        ),
        (
            "20230310T195032--fourier-transformation__math_physics.html",
            "20230310T195032",
            "fourier transformation",
            ['math', 'physics'],
        ),
        (
            "20000101T000000--general__economics.md",
            "20000101T000000",
            "general",
            ['economics'],
        ),
        (
            "20000101T000000--vim-grok-editing__development_editors_emacs_vim.jpg",
            "20000101T000000",
            "vim grok editing",
            ['development', 'editors', 'emacs', 'vim'],
        ),
        (
            "20000101T000000--postgresql__development_linux.org",
            "20000101T000000",
            "postgresql",
            ['development', 'linux'],
        ),
        (
            "20000101T000000--useful-programs__hobby_linux_ui.org",
            "20000101T000000",
            "useful programs",
            ['hobby', 'linux', 'ui'],
        ),
        (
            "20000101T000100--emacs__development_linux.org",
            "20000101T000100",
            "emacs",
            ['development', 'linux'],
        ),
        (
            "20000101T000000--tls-certificates__linux_shell_website.org",
            "20000101T000000",
            "tls certificates",
            ['linux', 'shell', 'website'],
        ),
        (
            "20000101T000000--tls-certificates.org",
            "20000101T000000",
            "tls certificates",
            [],
        ),
        (
            "20200604T093644--St.-Nikolaikirche-_-St.-Nikolai-church-_-Alter-Markt__monument_sky_spring.jpg",
            "20200604T093644",
            "St. Nikolaikirche _ St. Nikolai church _ Alter Markt",
            ["monument", "sky","spring"],
        ),

    ],
)
def test_filebase__get_info_from_filename(
        filename: str, date:str, name:str, tags:list[str]
):
    info = get_info_from_filename(filename)
    assert info
    print(info)
    assert (info[0].strftime('%Y%m%dT%H%M%S'), info[1], info[2]) == (date, name, tags)
