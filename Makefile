.PHONY: build run test lint clean

APP_NAME := sereserve
VERSION ?= latest

all: run-dev

build:
	docker build -t $(APP_NAME):$(VERSION) .

run-docker: build
	docker run -p 8000:8000 \
	-v /my/site/contents:/contents \
	-v /my/site/gallery:/gallery \
	$(APP_NAME)

run-dev:
	uvicorn presentation.app:app --port 8000 --reload

stop:
	docker stop $(APP_NAME)

test:
	python -m pytest -s --ignore=tests/integration

lint:
	python -m flake8 ./

clean:
	find . -name '*.pyc' -delete
	find . -name '__pycache__' -delete
	rm -rf build/
	rm -rf dist/
