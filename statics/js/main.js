const toggleBtn = document.getElementById("darkMode");
const body = document.body;
let darkMode = localStorage.getItem("dark-mode");

const enableDarkMode = () => {
    body.setAttribute('data-bs-theme','dark')
    localStorage.setItem("dark-mode", "enabled");
};

const disableDarkMode = () => {
    body.setAttribute('data-bs-theme','light')
    localStorage.setItem("dark-mode", "disabled");
};

if (darkMode === "enabled") {
    console.log('enabled');
    enableDarkMode();
} else {
    disableDarkMode();
    console.log('disabled');
}
toggleBtn.addEventListener("click", (e) => {
    darkMode = localStorage.getItem("dark-mode"); // update darkMode when clicked
    if (darkMode === "disabled") {
        enableDarkMode();
    } else {
        disableDarkMode();
    }
});
// make images responsive in the body
var images = body.getElementsByTagName("img");
for (var image of images) {
    image.className += " img-fluid" ;
}
