import toml
from pathlib import Path
from domain.models.config import Config


def from_file(toml_file: Path|str):
    if not isinstance(toml_file, Path):
        toml_file = Path(toml_file)
    assert (
        toml_file.exists() and toml_file.is_file()
    ), "Make sure config path is correct"
    config_dict = toml.loads(toml_file.read_text())
    return Config.parse_obj(config_dict)

config = from_file(Path('config.toml'))
