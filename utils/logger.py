import logging

from utils.config import config

base_logger = logging.getLogger(config.site.name)
base_logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - '
    '%(message)s', "%Y-%m-%d %H:%M")
ch.setFormatter(formatter)
ch.setLevel(config.site.log_level)
base_logger.addHandler(ch)
