import datetime
import re
from dataclasses import dataclass, field
from pathlib import Path

from domain.models.page import PageMetadata


def get_info_from_filename(
    filename, logger=None
) -> tuple[datetime.datetime, str, list[str]] | None:
    try:
        filename = '.'.join(filename.split('.')[:-1])
        info = re.split(r"--|__", filename)
        tags = info.pop().split("_") if len(info) == 3 else []
        title = " ".join(info.pop().split("-"))
        date = datetime.datetime.strptime(info.pop(), "%Y%m%dT%H%M%S")
        return date, title, tags
    except Exception as err:
        msg = f"error {err} while trying to get metadata from filename {filename}"
        if logger:
            logger.debug(msg)
        else:
            print(msg)
        return None


def get_datetime_from_file(path: Path, logger=None) -> datetime.datetime | None:
    found = None
    try:
        found = datetime.datetime.utcfromtimestamp(path.stat().st_ctime)
    except Exception as err:
        msg = f"error `{err}` in converting {path} to {format}"
        if logger:
            logger.debug(msg)
        else:
            print(msg)

    return found


def get_datetime_from_filename(
    filename: str, formats: list[str], logger=None
) -> datetime.datetime | None:
    found = None
    for format in formats[::-1]:
        try:
            sample = datetime.datetime(2000, 1, 1).strftime(format)
            found = datetime.datetime.strptime(filename[: len(sample)], format)
        except Exception as err:
            msg = f"error `{err}` in converting {filename} to {format}"
            if logger:
                logger.debug(msg)
            else:
                print(msg)
    return found
