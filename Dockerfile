FROM pandoc/core

RUN apk update && apk add python3 py-pip

WORKDIR /

COPY requirements/base.txt requirements/base.txt
RUN pip install --no-cache-dir -r requirements/base.txt

COPY . .

ENTRYPOINT ["uvicorn", "presentation.app:app", "--host", "0.0.0.0", "--port", "8000"]
