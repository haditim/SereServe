import dataclasses
from pathlib import Path
from domain.models.page import File
from re import sub

from dataclasses import dataclass, field
import datetime
from utils.logger import base_logger

logger = base_logger.getChild(__file__)


@dataclass
class AlbumMetadata:
    date: datetime.datetime | datetime.date | str | None = None
    title: str | None = None
    tags: list[str] = field(default_factory=list)
    _slug: str | None = None
    extra_keys: dict = field(default_factory=dict)

    def format_date(self, date_format: str) -> str:
        if self.date is None:
            return ""
        elif isinstance(self.date, str):
            return self.date
        try:
            return self.date.strftime(date_format)
        except Exception as err:
            logger.debug(f"error `{err}` in converting {self.date} to requested format")
        return ""

    def from_dict(self, dic: dict):
        for key, val in dic.items():
            if hasattr(self, key):
                setattr(self, key, val)
            else:
                if not hasattr(self, "extra_keys"):
                    self.extra_keys = {}
                self.extra_keys[key] = val

    @property
    def keywords(self) -> list[str]:
        return self.tags

    @keywords.setter
    def keywords(self, value: list[str]):
        self.tags = value

    @property
    def slug(self):
        if self._slug:
            return self._slug
        if self.title:
            return sub(
                r"[^\w-]",
                "",
                self.title.replace(" ", "_"),
            )
        return None

    @slug.setter
    def slug(self, value):
        self._slug = value


@dataclass
class GalleryItem:
    base_dir: Path
    relative_path: Path
    description: str = field(default=None, init=False)
    desc_file: File | None = None
    _metadata: AlbumMetadata = field(default_factory=AlbumMetadata)

    @property
    def metadata(self) -> AlbumMetadata:
        if self.desc_file:
            for field in dataclasses.fields(AlbumMetadata):
                if field in dataclasses.fields(self.desc_file.metadata) and (
                    desc_val := getattr(self.desc_file.metadata, field)
                ):
                    setattr(self._metadata, field, desc_val)
        return self._metadata

    @metadata.setter
    def metadata(self, value: AlbumMetadata):
        self._metadata = value


@dataclass
class Album(GalleryItem):
    pictures: list["Picture"] = field(default_factory=list)
    albums: list["Album"] = field(default_factory=list)

    @property
    def name(self) -> str:
        return str(self.relative_path.name)


@dataclass
class Picture(GalleryItem):
    created_date: datetime.datetime = field(default=None, init=False)
    tags: list[str] = field(default_factory=list)
    album: "Album | None" = None

    @property
    def name(self) -> str:
        return str(self.relative_path.stem)
