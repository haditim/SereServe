from pathlib import Path
from pydantic import BaseModel


class Site(BaseModel):
    name: str
    slogan: str
    description: str
    email: str
    copyright: str
    url: str
    log_level: str
    profile_image: str
    redirect_base_to: str | None
    tree_cache_time: int = 3600


class Server(BaseModel):
    port: int


class Content(BaseModel):
    class Template(BaseModel):
        export: str
        folder: str
        html: str

    base_path: Path
    export_path: Path
    static_files: list[str]
    content_files: list[str]
    code_highlight_style: str
    export_filename_append: str
    variable_datetime_formats: list[str]
    filename_datetime_formats: list[str]
    datetime_template_format: str
    enable_forwarding: bool = False
    dir_index_name: str = "index"
    force_export: bool = False
    template: Template


class Template(BaseModel):
    base_path: str
    sitemap: str = 'sitemap.html'
    sitemap_xml: str = 'sitemap.xml'
    rss: str = 'rss.rss'


class Static(BaseModel):
    base_path: Path
    serve_path: str


class Gallery(BaseModel):
    class Template(BaseModel):
        export: str
        galleries: str
        gallery: str
        picture: str

    serve_path: str
    base_path: Path
    templates_path: Path
    image_extensions: list[str]
    video_extensions: list[str]
    album_desc_file: str
    picture_desc_extension: str
    template: Template


class Config(BaseModel):
    site: Site
    server: Server
    content: Content
    template: Template
    static: Static
    gallery: Gallery
