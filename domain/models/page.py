from abc import abstractmethod
from copy import deepcopy
from pathlib import Path
import datetime
from enum import Enum
from dataclasses import dataclass, field
from typing import Iterable
from re import sub
from utils.logger import base_logger

logger = base_logger.getChild(__file__)


@dataclass
class PageMetadata:
    date: datetime.datetime | datetime.date | str | None = None
    title: str | None = None
    subtitle: str | None = None
    author: str | None = None
    num: bool = False
    tags: list[str] = field(default_factory=list)
    redirect_from: list[str] = field(default_factory=list)
    redirect_to: str | None = None
    thumbnail: str | None = None
    _slug: str | None = None
    extra_keys: dict = field(default_factory=dict)
    options: list[str] = field(default_factory=list)

    @abstractmethod
    def get_template(self, *args, **kwargs):
        ...

    def format_date(self, date_format: str) -> str:
        if self.date is None:
            return ""
        elif isinstance(self.date, str):
            return self.date
        try:
            return self.date.strftime(date_format)
        except Exception as err:
            logger.debug(f"error `{err}` in converting {self.date} to requested format")
        return ""

    @property
    def iso_date(self) -> str:
        if self.date is None:
            return ""
        elif isinstance(self.date, str):
            return self.date
        try:
            return self.date.isoformat()
        except Exception as err:
            logger.debug(f"error `{err}` in converting {self.date} to requested format")
        return ""


    def from_dict(self, dic: dict):
        for key, val in dic.items():
            if key == "tags" or key == "keywords":
                if isinstance(val, list):
                    val = " ".join(val)
                self.tags = val.split(" ")

            elif hasattr(self, key):
                setattr(self, key, val)

            else:
                if not hasattr(self, "extra_keys"):
                    self.extra_keys = {}
                self.extra_keys[key] = val

    @property
    def keywords(self) -> list[str]:
        return self.tags

    @keywords.setter
    def keywords(self, value: list[str]):
        self.tags = value

    @property
    def slug(self):
        if self._slug:
            return self._slug
        if self.title:
            return sub(
                r"[^\w-]",
                "",
                self.title.replace(" ", "_"),
            )
        return None

    @slug.setter
    def slug(self, value):
        self._slug = value


class FileType(str, Enum):
    MARKDOWN = "markdown"
    ORG = "org"
    HTML = "html"
    UNSUPPORTED = "unsupported"


@dataclass
class Page:
    base_dir: Path
    relative_path: Path
    parent_folder: "Folder | None" = None
    output_path: Path = field(default=None, init=False)
    thumbnail: Path = field(default=None, init=False)
    _metadata: PageMetadata = field(default_factory=PageMetadata)
    _content: str | None = None

    def __lt__(self, other):
        if (
            (this := self.metadata.date)
            and (that := other.metadata.date)
            and isinstance(this, datetime.datetime)
            and isinstance(that, datetime.datetime)
        ):
            return this < that
        return self.full_path < other.full_path

    def __post_init__(self):
        self._get_metadata()

    @property
    def metadata(self) -> PageMetadata:
        return self._metadata

    @metadata.setter
    def metadata(self, value: PageMetadata):
        self._metadata = value

    def _get_metadata(self):
        self._metadata.from_dict(
            {
                "slug": sub(r"[^\w-]", "", self.full_path.stem.replace(" ", "_")),
                "title": self.full_path.stem.capitalize(),
            }
        )

    @property
    def exported_content(self) -> str | None:
        if self._content:
            return self._content
        if not self.output_path:
            return None
        try:
            return self.output_path.read_text()
        except Exception as err:
            logger.error(f"error getting exported content. Error: {err}")
            return None

    @exported_content.setter
    def exported_content(self, content: str):
        self._content = content

    @property
    def sluged_path(self):
        sluged = []
        entity = self
        while entity.parent_folder:
            sluged.append(entity._metadata.slug)
            entity = entity.parent_folder
        return "/".join(sluged[::-1])

    @property
    def full_title(self):
        title = []
        entity = self
        while entity.parent_folder:
            if (
                not entity.parent_folder.index_file
                and not entity.parent_folder.has_single_file
            ):
                title.append(entity.metadata.title)
            entity = entity.parent_folder
        title.append(entity.metadata.title)
        # remove "contents" from title
        return " → ".join(title[::-1][1:])

    @property
    def breadcrumb(self) -> list["Page"]:
        breadcrumb = []
        entity = self
        while entity.parent_folder:
            breadcrumb.append(entity)
            entity = entity.parent_folder
        return breadcrumb[::-1]

    @property
    def thumbnail_sluged_path(self):
        if self.metadata.thumbnail:
            if isinstance(self, Folder):
                if self.index_file or self.has_single_file:
                    return Path(self.sluged_path) / self.metadata.thumbnail
            if self.parent_folder:
                return Path(self.parent_folder.sluged_path) / self.metadata.thumbnail
        return None

    @property
    def full_path(self):
        return self.base_dir / self.relative_path

    def get_parent_folders(self) -> list["Folder"]:
        """Get a list of all parent folders of this item"""
        if self.parent_folder is None:
            return []
        else:
            return self.parent_folder.get_parent_folders() + [self.parent_folder]

    def __repr__(self):
        return f"Page site path: {self.sluged_path}, directory path: {self.full_path}"


@dataclass
class Folder(Page):
    folders: list["Folder"] = field(default_factory=list)
    files: list["File"] = field(default_factory=list)
    index_file: "File | None" = field(default=None, init=False)

    @property
    def metadata(self):
        if self.index_file:
            return self.index_file.metadata
        if self.has_single_file:
            return self.files[0].metadata
        return self._metadata

    @property
    def has_single_file(self) -> bool:
        return len(self.files) == 1 and not self.folders

    @property
    def sorted(self) -> list["File"]:
        """File and folders which should be shown, sorted."""

        def get_folder_files(folder: "Folder") -> list["File"]:
            _return_list = sorted([*folder.files, *folder.folders], reverse=True)
            return _return_list

        return get_folder_files(self)

    @property
    def flattened(self) -> "Folder":
        """Files in all subdirs, flattened and sorted."""
        folder = Folder(self.base_dir, self.relative_path)

        def get_folder_files(folder: "Folder") -> tuple[list["File"], list["Folder"]]:
            _return_files = [*folder.files]
            _return_folders = []
            for folder in folder.folders:
                if folder.index_file or folder.has_single_file:
                    _return_folders.append(folder)
                else:
                    _files, _folders = get_folder_files(folder)
                    _return_files.extend(_files)
                    _return_folders.extend(_folders)
            return sorted(_return_files, reverse=True), sorted(
                _return_folders, reverse=True
            )

        folder.files, folder.folders = get_folder_files(self)

        return folder


@dataclass
class File(Page):
    type: FileType = field(default=FileType.UNSUPPORTED, init=False)
