from pathlib import Path
from domain.models.gallery import Album
from domain.repositories.gallery import GalleryRepo

from utils.logger import base_logger

logger = base_logger.getChild(__name__)


class GalleryUseCase:
    def __init__(self, repo: GalleryRepo):
        self.gallery_repository = repo

    def get_root_tree(self):
        return self.gallery_repository.root_tree

    def get_pictures_by_tag(self, tag: str):
        return self.gallery_repository.pictures_by_tag(tag)

    def get_album_by_name(self, album_name: str):
        return self.gallery_repository.get_album_by_name(album_name)

    def get_picture_by_name(self, album: Album, picture_name: str):
        return self.gallery_repository.get_picture_by_name(album, picture_name)
