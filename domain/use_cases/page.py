from pathlib import Path
from domain.models.page import Page
from domain.repositories.page import PageRepo

from utils.logger import base_logger

logger = base_logger.getChild(__name__)


class PageUseCase:
    def __init__(self, repo: PageRepo):
        self.file_repository = repo

    @property
    def last_built(self):
        return self.file_repository.last_built

    def get_root_tree(self):
        return self.file_repository.root_tree

    def get_tree_by_tag(self, tag: str):
        return self.file_repository.tree_by_tag(tag)

    def find_by_sluged_path(self, sluged_path: str):
        return self.file_repository.find_by_sluged_path(sluged_path)
