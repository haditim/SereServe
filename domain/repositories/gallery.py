from abc import ABC, abstractmethod
from pathlib import Path

from domain.models.gallery import Album, Picture


class GalleryRepo(ABC):
    @property
    @abstractmethod
    def root_tree(self):
        pass

    @abstractmethod
    def pictures_by_tag(self, tag: str):
        pass

    @abstractmethod
    def get_album_by_name(self, album_name: str):
        pass

    @abstractmethod
    def get_picture_by_name(self, album: Album, picture_name: str):
        pass
