from abc import ABC, abstractmethod
from pathlib import Path

from domain.models.page import Page


class PageRepo(ABC):
    last_built: float

    @property
    @abstractmethod
    def root_tree(self):
        pass

    @abstractmethod
    def tree_by_tag(self, tag: str):
        pass

    @abstractmethod
    def find_by_sluged_path(self, sluged_path: str):
        pass
