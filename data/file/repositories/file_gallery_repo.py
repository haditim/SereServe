import time
from datetime import datetime
from pathlib import Path

from domain.models.gallery import Album, Picture
from domain.models.page import File
from data.file.models import org, md, html
from domain.repositories.gallery import GalleryRepo
from utils.logger import base_logger
from utils.config import config
from utils.files import get_info_from_filename

logger = base_logger.getChild(__file__)


class FilePictureRepo(GalleryRepo):
    def __init__(
        self,
        base_dir: Path,
        cache_timeout=config.site.tree_cache_time,
        extensions: list[str] | None = None,
        picture_desc_format: str | None = None,
        album_desc_file: str | None = None,
    ):
        self.base_dir = Path(base_dir)
        self.extensions = extensions if extensions else []
        self.cache_timeout = cache_timeout
        self.picture_desc_format = picture_desc_format
        self.album_desc_file = album_desc_file
        self._root_tree: Album | None = None
        self._last_populated: float = 0

    @property
    def root_tree(self) -> Album | None:
        if (
            not self._root_tree
            or time.time() - self._last_populated > self.cache_timeout
        ):
            logger.debug(f'recreating root_tree, time diff {time.time() - self._last_populated}')
            self._last_populated = time.time()
            self._root_tree = self._build_directory(Path("."))
        return self._root_tree

    @staticmethod
    def _prepare_desc_file(path: Path, base_dir: Path):
        file = File(
            base_dir=base_dir,
            relative_path=path.relative_to(base_dir),
        )
        for file_type in (md.MarkdownFile, org.OrgFile, html.HTMLFile):
            if path.suffix in file_type.extensions:
                file = file_type(
                    base_dir=base_dir,
                    relative_path=path.relative_to(base_dir),
                    export_template=f"{config.template.base_path}/{config.gallery.template.export}",
                )
        return file

    def _prepare_picture(self, path: Path, album: Album | None = None) -> Picture:
        desc_file = path.with_suffix(self.picture_desc_format)
        if desc_file.exists():
            pic = Picture(
                base_dir=self.base_dir,
                relative_path=path.relative_to(self.base_dir),
                desc_file=self._prepare_desc_file(desc_file, self.base_dir),
                album=album,
            )
        else:
            pic = Picture(
                base_dir=self.base_dir,
                relative_path=path.relative_to(self.base_dir),
                album=album,
            )
        if info := get_info_from_filename(path.parts[-1]):
            pic.metadata.date, pic.metadata.title, pic.metadata.tags = info
        return pic

    @staticmethod
    def _file_should_be_processed(path: Path, extensions: list[str]):
        return not extensions or path.suffix.lower() in extensions

    def _build_directory(self, relative_dir: Path) -> Album | None:
        album_desc_file = self.base_dir / relative_dir / self.album_desc_file
        if album_desc_file.exists():
            album = Album(
                base_dir=self.base_dir,
                relative_path=relative_dir,
                desc_file=self._prepare_desc_file(album_desc_file, self.base_dir),
            )
        else:
            album = Album(
                base_dir=self.base_dir,
                relative_path=relative_dir,
            )
        directory_path = self.base_dir / relative_dir

        for item in directory_path.iterdir():
            if item.is_dir():
                if nested_album := self._build_directory(
                    item.relative_to(self.base_dir)
                ):
                    album.albums.append(nested_album)

                continue

            if not self._file_should_be_processed(item, self.extensions):
                continue

            album.pictures.append(self._prepare_picture(item, album))

        return album if album.albums or album.pictures else None

    def pictures_by_tag(self, tag: str) -> Album:
        album = Album(self.base_dir, Path("tag"))

        for alb in self.root_tree.albums:
            for picture in alb.pictures:
                if tag.lower() in [t.lower() for t in picture.metadata.tags]:
                    album.pictures.append(picture)
        for picture in self.root_tree.pictures:
            if tag.lower() in [t.lower() for t in picture.metadata.tags]:
                album.pictures.append(picture)

        return album

    def get_album_by_name(self, album_name: str) -> Album | None:
        for album in self.root_tree.albums:
            if album.name.lower() == album_name.lower():
                return album
        return None

    def get_picture_by_name(self, album: Album, picture_name: str) -> Picture | None:
        for picture in album.pictures:
            if picture.name.lower() == picture_name.lower():
                return picture
        return None
