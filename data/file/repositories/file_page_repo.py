import time
from copy import deepcopy
from pathlib import Path

from data.file.models import folder, html, md, org
from domain.models.page import File, Folder, Page
from domain.repositories.page import PageRepo
from utils.config import config
from utils.files import get_info_from_filename
from utils.logger import base_logger

logger = base_logger.getChild(__file__)


class FilePageRepo(PageRepo):
    def __init__(
        self,
        base_dir: Path,
        cache_timeout=config.site.tree_cache_time,
        extensions: list[str] | None = None,
    ):
        self.base_dir = Path(base_dir)
        self.extensions = extensions if extensions else []
        self.cache_timeout = cache_timeout
        self.last_built: float = 0
        self._root_tree: Folder | None = None

    @property
    def root_tree(self) -> Folder | None:
        if not self._root_tree or time.time() - self.last_built > self.cache_timeout:
            self.last_built = time.time()
            self._root_tree = self._build_directory(Path("."))
        return self._root_tree

    @staticmethod
    def _file_should_be_processed(path: Path, extensions: list[str]):
        return not extensions or path.suffix.lower() in extensions

    @staticmethod
    def _file_is_exportee(path: Path):
        return config.content.export_filename_append in path.stem

    @staticmethod
    def _prepare_file(path: Path, base_dir: Path, parent_menu: Folder):
        file = File(
            base_dir=base_dir,
            relative_path=path.relative_to(base_dir),
            parent_folder=parent_menu,
        )
        for file_type in (md.MarkdownFile, org.OrgFile, html.HTMLFile):
            if path.suffix in file_type.extensions:
                file = file_type(
                    base_dir=base_dir,
                    relative_path=path.relative_to(base_dir),
                    parent_folder=parent_menu,
                )
        if info := get_info_from_filename(file.full_path.parts[-1]):
            file.metadata.date, file.metadata.title, file.metadata.tags = info
        return file

    def _build_directory(self, relative_dir: Path) -> Folder | None:
        menu = folder.ContentFolder(self.base_dir, relative_dir)
        directory_path = self.base_dir / relative_dir

        for item in directory_path.iterdir():
            if item.is_dir():
                if fol := self._build_directory(
                    item.relative_to(self.base_dir),
                ):
                    fol.parent_folder = menu
                    menu.folders.append(fol)
                continue

            if not self._file_should_be_processed(item, self.extensions):
                continue

            if self._file_is_exportee(item):
                continue

            menu.files.append(self._prepare_file(item, self.base_dir, menu))

        if not menu.folders and not menu.files:
            return None
        return menu

    def tree_by_tag(self, tag: str, folder: Folder | None = None) -> Folder:
        if not folder:
            folder = deepcopy(self.root_tree)

        for fol in folder.folders:
            if not self.tree_by_tag(tag, fol):
                folder.folders.remove(fol)

        tagged_files = []
        for file in folder.files:
            if tag.lower() in [t.lower() for t in file.metadata.tags]:
                tagged_files.append(file)

        folder.files = tagged_files
        return folder

    def find_by_sluged_path(
        self, sluged_path: str, folder: Folder | Path | None = None
    ) -> Page | None:
        if not folder:
            folder = self.root_tree
        if not folder:
            return None
        if not (subs := sluged_path.split("/")):
            # '/' or ''
            return folder

        for ind, sub in enumerate(subs):
            for fol in folder.folders:
                if fol.metadata.slug == sub or fol._metadata.slug == sub:
                    folder = fol
                    continue

        if folder.sluged_path == sluged_path:
            return folder

        remaining = sluged_path[len(folder.sluged_path) :].strip("/")
        for file in folder.files:
            if file.sluged_path == sluged_path:
                return file

            # remove file slug from what we want
            if file.sluged_path in sluged_path:
                remaining = sluged_path[len(file.sluged_path) :].strip("/")

        if (p := folder.full_path / remaining).exists():
            return p

        return None
