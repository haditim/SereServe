import datetime
import subprocess
from abc import abstractmethod
from dataclasses import dataclass, field
from pathlib import Path

from domain.models.page import File, FileType
from jinja2 import Environment, FileSystemLoader
from utils import config, files
from utils.logger import base_logger

conf = config.from_file(Path("config.toml"))
logger = base_logger.getChild(__file__)


@dataclass
class FileBase(File):
    type = FileType.UNSUPPORTED
    export_template: str = field(
        default=f"{conf.template.base_path}/{conf.content.template.export}"
    )

    def get_template(self, template_base_path: Path):
        env = Environment()
        env.loader = FileSystemLoader(
            [
                (template_base_path),
                self.output_path.parent,
            ]
        )
        return env.get_template(self.output_path.name)

    def __post_init__(self):
        self._get_metadata()
        self._set_date()

    @abstractmethod
    def _get_metadata(self):
        ...

    def _set_date(self):
        if dt := self.metadata.date:
            if isinstance(dt, str):
                for format in conf.content.variable_datetime_formats[::-1]:
                    try:
                        self.metadata.date = datetime.datetime.strptime(dt, format)
                    except Exception as err:
                        logger.debug(
                            f"could not convert {dt} with format {format}, error: `{err}`"
                        )

        elif dt := files.get_datetime_from_filename(
            self.full_path.stem, conf.content.filename_datetime_formats
        ):
            self.metadata.date = dt
        elif dt := files.get_datetime_from_file(self.full_path):
            self.metadata.date = dt
        else:
            logger.error(f"could not set date for {self.full_path}")
        logger.error(f"file {self.full_path} date is {self.metadata.date}")

    @property
    def output_path(self) -> Path:
        if export_path := conf.content.export_path:
            output = export_path / self.full_path.relative_to(self.base_dir.parent)
            output = output.with_suffix(f"{conf.content.export_filename_append}.html")
        else:
            output = self.full_path.with_suffix(
                f"{conf.content.export_filename_append}.html"
            )

        if (
            not conf.content.force_export
            and output.exists()
            and output.stat().st_mtime >= self.full_path.stat().st_mtime
        ):
            return output
        output.parent.mkdir(parents=True, exist_ok=True)
        cmd = (
            f"pandoc -f {self.type.value} -s -t html5 --mathjax --toc "
            f"--citeproc --resource-path {self.full_path.parent} "
            "--metadata link-citations=true --metadata link-bibliography=true "
            f"--template {self.export_template} "
            f"{self.full_path} -o {output} "
        )
        if 'num' in self.metadata.options:
            cmd += "--number-sections "
        style = conf.content.code_highlight_style
        if style:
            cmd += f"--highlight-style {style} "
        result = subprocess.run(cmd, shell=True)
        if not result.returncode == 0:
            logger.error(
                f"command {cmd} failed, result: {result}, \nstdout: {result.stdout} \nstderr: {result.stderr}"
            )
            raise Exception(
                "An error occured in export functionality. Please check the logs."
            )
        return output
