from dataclasses import dataclass
from pathlib import Path

from domain.models.page import File, Folder
from jinja2 import Environment, FileSystemLoader
from utils.config import config


@dataclass
class ContentFolder(Folder):
    def get_template(self, template_base_path: Path):
        env = Environment()
        if self.index_file:
            env.loader = FileSystemLoader(
                [
                    (template_base_path),
                    self.index_file.output_path.parent,
                ]
            )
            return env.get_template(self.index_file.output_path.name)
        else:
            env.loader = FileSystemLoader([(template_base_path)])
            return env.get_template(config.content.template.folder)

    @property
    def index_file(self) -> File | None:
        if dir_index_name := config.content.dir_index_name:
            for file in self.files:
                if dir_index_name in file.relative_path.stem:
                    return file
        if self.has_single_file:
            return self.files[0]
        return None
