from dataclasses import dataclass

from data.file.models.filebase import FileBase
from domain.models.page import FileType, PageMetadata
from utils.logger import base_logger

logger = base_logger.getChild(__file__)


@dataclass
class OrgFile(FileBase):
    type = FileType.ORG
    extensions = [".org", ".orgmode"]

    def __post_init__(self):
        super().__post_init__()
        logger.error(f"file {self.full_path} date is {self.metadata.date}")

    def _get_metadata(self):
        self.metadata = PageMetadata()
        content = self.full_path.read_text()
        options = {}
        for line in content.split("\n"):
            if not line.startswith("#+"):
                break
            key, value = line[2:].split(":", 1)
            key = key.strip().lower()
            value = value.strip()
            if prev_val := options.get(key):
                if isinstance(prev_val, list):
                    options[key].append(value)
                else:
                    options[key] = [prev_val, value]
            else:
                options[key] = value

        options["mathjax"] = True

        self.metadata.from_dict(options)
