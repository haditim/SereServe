from dataclasses import dataclass
from pathlib import Path

from data.file.models.filebase import FileBase
from domain.models.page import FileType, PageMetadata
from jinja2 import Environment, FileSystemLoader
from utils.config import config


@dataclass
class HTMLFile(FileBase):
    type = FileType.HTML
    extensions = [".html", ".htm"]

    def get_template(self, template_base_path: Path):
        env = Environment()
        env.loader = FileSystemLoader([(template_base_path)])
        return env.get_template(config.content.template.html)

    @property
    def output_path(self) -> Path:
        return self.full_path

    def _get_metadata(self):
        self.metadata = PageMetadata()
        options = {}
        for line in self.full_path.read_text().splitlines():
            if "<title>" in line:
                options["title"] = line[
                    line.find(">") + 1 : line.find("<", line.find(">") + 1)
                ]
            if "<meta" in line:
                splitted = line.strip().split('"')
                key, val = None, None
                for ind, item in enumerate(splitted):
                    if "name" in item:
                        key = splitted[ind + 1]
                    if "content" in item:
                        val = splitted[ind + 1]
                if key and val:
                    options[key] = val
        options["mathjax"] = True
        print(f'getting metadata for {self.full_path}, title: {options["title"]}')
        self.metadata.from_dict(options)
