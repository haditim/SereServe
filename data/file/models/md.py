from dataclasses import dataclass

from data.file.models.filebase import FileBase
from domain.models.page import FileType, PageMetadata


@dataclass
class MarkdownFile(FileBase):
    type = FileType.MARKDOWN
    extensions = [".md", ".markdown"]

    def __post_init__(self):
        self._get_metadata()
        super().__post_init__()

    def _get_metadata(self):
        self.metadata = PageMetadata()
        parts = self.full_path.read_text().split("---\n", 2)

        options = {}
        if len(parts) > 2:
            metadata_lines = parts[1].split("\n")
            for line in metadata_lines:
                key_val = line.split(": ", 1)
                if len(key_val) == 2:
                    key, value = key_val
                    if prev_val := options.get(key.strip().lower()):
                        if isinstance(prev_val, list):
                            options[key.strip().lower()].append(value.strip())
                        else:
                            options[key.strip().lower()] = [prev_val, value.strip()]
                    else:
                        options[key.strip().lower()] = value.strip()

        options["mathjax"] = True
        self.metadata.from_dict(options)
