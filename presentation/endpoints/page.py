from pathlib import Path

from data.file.repositories.file_page_repo import FilePageRepo
from domain.models.page import Folder
from domain.use_cases.page import PageUseCase
from fastapi import HTTPException, Request, Response
from fastapi.responses import FileResponse, RedirectResponse, HTMLResponse
from fastapi.routing import APIRouter
from fastapi.templating import Jinja2Templates
from starlette import status
from utils.config import config
from utils.logger import base_logger

logger = base_logger.getChild("presentation.endpoints.page")

router = APIRouter()

content_base_path = Path(config.content.base_path)

repo = FilePageRepo(content_base_path, extensions=config.content.content_files)
usecase = PageUseCase(repo)

templates = Jinja2Templates(config.template.base_path)

assert usecase.get_root_tree, "site must have content"


@router.get("/")
def redirect_base():
    return RedirectResponse(
        url=config.site.redirect_base_to, status_code=status.HTTP_307_TEMPORARY_REDIRECT
    )


@router.get("/tag/{tag}")
def get_pages_with_tag(request: Request, tag: str):
    page = usecase.get_tree_by_tag(tag)
    try:
        return HTMLResponse(
            page.get_template(config.template.base_path).render(
                {
                    "request": request,
                    "site": config.site,
                    "page": page.flattened,
                    "config": config,
                    "tag": tag,
                }
            )
        )



    except Exception as err:
        logger.error(f"`{err}` occured when trying to serve {page.sluged_path}")
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occured, please check logs.",
        )


@router.get("/{targetpath:path}/")
def get_page_content(request: Request, targetpath: str, redirect_from: str = ""):
    root_tree = usecase.get_root_tree()
    page = usecase.find_by_sluged_path(targetpath)
    if isinstance(page, Path) and page.suffix in config.content.static_files:
        return FileResponse(page)

    if not page:
        if config.content.enable_forwarding:
            return forward_or_404(root_tree, targetpath)
    if isinstance(page, Folder) and (
        redirect := forward_if_single_file_in_folder(page, redirect_from)
    ):
        return redirect

    try:
        return HTMLResponse(
            page.get_template(config.template.base_path).render(
                {
                    "request": request,
                    "site": config.site,
                    "page": page,
                    "redirect_from": redirect_from,
                    "config": config,
                }
            )
        )
    except Exception as err:
        logger.error(f"`{err}` occured when trying to serve {page.sluged_path}")
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occured, please check logs.",
        )


def forward_or_404(site_menu: Folder, targetpath: str):
    for item in site_menu.flattened.sorted:
        if (
            isinstance(item.metadata.redirect_from, str)
            and targetpath.strip("/") == item.metadata.redirect_from.strip("/")
        ) or (
            isinstance(item.metadata.redirect_from, list)
            and targetpath.strip("/") in item.metadata.redirect_from
        ):
            url = router.url_path_for("get_page_content", targetpath=item.sluged_path)
            url += f"?redirect_from={targetpath}"
            return RedirectResponse(
                url=url,
                status_code=status.HTTP_301_MOVED_PERMANENTLY,
            )
    raise HTTPException(status.HTTP_404_NOT_FOUND)


def forward_if_single_file_in_folder(item: Folder, redirect_from: str = None):
    if not item.has_single_file and not item.index_file:
        return
    if item.has_single_file:
        url = router.url_path_for(
            "get_page_content", targetpath=item.files[0].sluged_path
        )
    elif item.index_file:
        url = router.url_path_for(
            "get_page_content", targetpath=item.index_file.sluged_path
        )
    if redirect_from:
        url += f"?redirect_from={redirect_from}"
    return RedirectResponse(
        url=url,
        status_code=status.HTTP_307_TEMPORARY_REDIRECT,
    )
