from pathlib import Path

from data.file.repositories.file_gallery_repo import FilePictureRepo
from domain.use_cases.gallery import GalleryUseCase
from fastapi import HTTPException, Request
from fastapi.responses import FileResponse
from fastapi.routing import APIRouter
from fastapi.templating import Jinja2Templates
from starlette.status import HTTP_404_NOT_FOUND
from utils.config import config
from utils.logger import base_logger

logger = base_logger.getChild("presentation.endpoints.page")

router = APIRouter(redirect_slashes=True)

gallery_base_path = Path(config.gallery.base_path)

repo = FilePictureRepo(
    base_dir=config.gallery.base_path,
    extensions=config.gallery.image_extensions + config.gallery.video_extensions,
    picture_desc_format=config.gallery.picture_desc_extension,
    album_desc_file=config.gallery.album_desc_file,
)
usecase = GalleryUseCase(repo)

templates = Jinja2Templates(config.template.base_path)


@router.get("/")
@router.get("")
def get_albums(request: Request):
    root_tree = usecase.get_root_tree()
    return templates.TemplateResponse(
        config.gallery.template.galleries,
        {
            "request": request,
            "gallery": root_tree,
            "site": config.site,
            "gallery_conf": config.gallery,
        },
    )


@router.get("/tag/{tag}")
def get_pictures_with_tag(request: Request, tag: str):
    album = usecase.get_pictures_by_tag(tag)
    try:
        return templates.TemplateResponse(
            config.gallery.template.gallery,
            {
                "request": request,
                "album": album,
                "site": config.site,
                "gallery_conf": config.gallery,
                "tag": tag
            },
        )
    except Exception as err:
        raise HTTPException(HTTP_404_NOT_FOUND, detail=str(err))


@router.get("/{album_name}")
def get_gallery_album(request: Request, album_name: str):
    if album := usecase.get_album_by_name(album_name):
        return templates.TemplateResponse(
            config.gallery.template.gallery,
            {
                "request": request,
                "album": album,
                "site": config.site,
                "gallery_conf": config.gallery,
            },
        )
    raise HTTPException(HTTP_404_NOT_FOUND)


@router.get("/{album_name}/{picture_name}")
def get_picture_content(request: Request, album_name: str, picture_name: str):
    picture_path = Path(album_name) / Path(picture_name)
    full_path = config.gallery.base_path / picture_path
    if full_path.is_file():
        return FileResponse(full_path)
    album = usecase.get_album_by_name(album_name)
    if not album:
        raise HTTPException(HTTP_404_NOT_FOUND)
    picture = usecase.get_picture_by_name(album, picture_name)
    if not picture:
        raise HTTPException(HTTP_404_NOT_FOUND)
    return templates.TemplateResponse(
        config.gallery.template.picture,
        {
            "request": request,
            "album": album,
            "picture": picture,
            "site": config.site,
            "gallery_conf": config.gallery,
        },
    )
