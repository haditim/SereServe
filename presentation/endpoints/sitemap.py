from pathlib import Path

from data.file.repositories.file_gallery_repo import FilePictureRepo
from data.file.repositories.file_page_repo import FilePageRepo
from domain.use_cases.gallery import GalleryUseCase
from domain.use_cases.page import PageUseCase
from fastapi import Request, Response
from fastapi.routing import APIRouter
from fastapi.templating import Jinja2Templates
from jinja2 import Environment, FileSystemLoader
from utils.config import config
from utils.logger import base_logger

logger = base_logger.getChild("presentation.endpoints.sitemap")
content_base_path = Path(config.content.base_path)
gallery_base_path = Path(config.gallery.base_path)
content_repo = FilePageRepo(content_base_path, extensions=config.content.content_files)
gallery_repo = FilePictureRepo(
    base_dir=config.gallery.base_path,
    extensions=config.gallery.image_extensions + config.gallery.video_extensions,
    picture_desc_format=config.gallery.picture_desc_extension,
    album_desc_file=config.gallery.album_desc_file,
)
content_usecase = PageUseCase(content_repo)
gallery_usecase = GalleryUseCase(gallery_repo)

router = APIRouter(redirect_slashes=True)
templates = Jinja2Templates(config.template.base_path)


@router.get("/robots.txt")
def get_robots(request: Request):
    return Response(
        f"""
Sitemap: {request.url_for('get_sitemap_xml')}
User-agent: CCBot
Disallow: /

User-agent: ChatGPT-User
Disallow: /

User-agent: GPTBot
Disallow: /

User-agent: Google-Extended
Disallow: /

User-agent: Omgilibot
Disallow: /

User-agent: Omgili
Disallow: /

User-agent: FacebookBot
Disallow: /
   """,
        media_type="text/plain",
    )

@router.get("/sitemap.xml")
def get_sitemap_xml(request: Request):
    gallery_root_tree = gallery_usecase.get_root_tree()
    content_root_tree = content_usecase.get_root_tree()
    env = Environment()
    env.loader = FileSystemLoader(
        [
            (config.template.base_path),
        ]
    )
    template = env.get_template(config.template.sitemap_xml)
    return Response(
        content=template.render(
            {
                "request": request,
                "content": content_root_tree,
                "gallery": gallery_root_tree,
                "site": config.site,
                "content_last_built": content_usecase.last_built,
                "config": config,
            }
        ),
        media_type="text/xml",
    )

@router.get("/sitemap")
def get_sitemap(request: Request):
    gallery_root_tree = gallery_usecase.get_root_tree()
    content_root_tree = content_usecase.get_root_tree()
    return templates.TemplateResponse(
        config.template.sitemap,
        {
            "request": request,
            "gallery": gallery_root_tree,
            "content": content_root_tree,
            "site": config.site,
            "content_last_built": content_usecase.last_built,
            "config": config,
        },
    )


@router.get("/rss.rss")
def get_rss(request: Request):
    content_root_tree = content_usecase.get_root_tree()
    env = Environment()
    env.loader = FileSystemLoader(
        [
            (config.template.base_path),
        ]
    )
    template = env.get_template(config.template.rss)
    return Response(
        content=template.render(
            {
                "request": request,
                "content": content_root_tree,
                "site": config.site,
                "content_last_built": content_usecase.last_built,
                "config": config,
            }
        ),
        media_type="application/rss+xml",
    )
