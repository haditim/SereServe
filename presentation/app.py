#!/usr/bin/env python3
from fastapi import FastAPI, staticfiles, status
from presentation.endpoints.page import router as page_router
from presentation.endpoints.gallery import router as gallery_router
from presentation.endpoints.sitemap import router as sitemap_router
from utils.config import config
from fastapi.templating import Jinja2Templates

templates = Jinja2Templates(directory=config.template.base_path)

app = FastAPI(docs_url=None)
app.mount(
    config.static.serve_path,
    staticfiles.StaticFiles(directory=config.static.base_path),
    name="static",
)
app.include_router(gallery_router, prefix=config.gallery.serve_path)
app.include_router(page_router, prefix="")
app.include_router(sitemap_router, prefix="")


@app.exception_handler(status.HTTP_404_NOT_FOUND)
async def custom_404_handler(request, exc):
    return templates.TemplateResponse(
        "404.html",
        {
            "request": request,
            "exc": exc,
            "request": request,
            "site": config.site,
        },
        status_code=404,
    )

@app.exception_handler(status.HTTP_500_INTERNAL_SERVER_ERROR)
async def custom_500_handler(request, exc):
    return templates.TemplateResponse(
        "500.html",
        {
            "request": request,
            "exc": exc,
            "request": request,
            "site": config.site,
        },
        status_code=500,
    )
